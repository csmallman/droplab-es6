import isDropdownParts from './utils/is_dropdown_parts';
import toDataCamelCase from './utils/to_data_camelcase';
import closestParent from './utils/closest_parent';
import HookInput from './hooks/hook_input';
import HookButton from './hook/hook_button';
import HookFactory from './hooks/factory';

HookFactory.registerHook('a', HookButton);
HookFactory.registerHook('button', HookButton);
HookFactory.registerHook('input', HookInput);

let instance;

class DropLab {
  constructor(hooks = []) {
    if(!instance) {
      instance = this;
    }

    this.ready = false;
    this.hooks = [];
    this.queuedData = [];
    this.config = {};
    this.plugins = [];

    if (hooks.length) {
      this.addHooks(hooks);
    }

    return instance;
  }

  start() {
    // Invoke each plugin callback, passing constructor
    for (const cb of this.plugins) {
      cb(DropLab);
    }

    // Create custom read event and dispatch
    const readyEvent = new CustomEvent('ready.dl', {
      detail: {
        dropdown: this,
      },
    });

    window.dispatchEvent(readyEvent);

    // Add events
    this.listen();

    // Set ready to true
    this.ready = true;

    // Add any queued data and reset queuedData list
    for (const args of this.queuedData) {
      this.addData(...args);
    }
    this.queuedData = [];
  }

  listen() {
    window.addEventListener('click', this, false);
  }

  plugin(callback) {
    this.plugins.push(callback);
  }

  addData(...args) {
    this.applyArgs(args, '_addData');
  }

  setData(...args) {
    this.applyArgs(args, '_setData');
  }

  applyArgs(args, methodName) {
    if(this.ready) {
      this[methodName](...args);
    } else {
      this.queuedData = this.queuedData || [];
      this.queuedData.push(args);
    }
  }

  changeHookList(id, listSelector) {
    const trigger = document.querySelector(`[data-id="${id}"]`);
    const list = document.querySelector(listSelector);

    for (const [index, hook] of this.hooks.entries()) {
      if(hook.trigger === trigger) {
        this.distroyHook(hook);
        this.hooks.splice(i, 1);
        this.addHook(trigger, list);
      }
    }
  }

  addHook(hook, list) {
    const hookElement = (!(hook instanceof HTMLElement) && typeof hook === 'string') ? document.querySelector(hook) : hook;
    const listElement = (list) ? list : document.querySelector(hook.dataset[toDataCamelCase('data-dropdown-trigger')]);

    this.hooks.push(HookFactory.getHook(hook.tagName.toLowerCase(), hookElement, listElement));

    return this;
  }

  addHooks(hooks) {
    for (const hook of hooks) {
      this.addHook(hook);
    }

    return this;
  }

  setConfig(obj){
    this.config = obj;
  }

  destroy() {
    for (const hook of this.hooks) {
      this.destroyHook(hook);
    }

    this.hooks = [];

    window.removeEventListener('click', this, false);
  }

  destroyHook(hook) {
    hook.destroy();
  }

  handleEvent(ev) {
    const tagName = ev.target.tagName;
    const thisTag = (tagName === 'LI' || tagName === 'A') ? ev.target : closestParent(ev.target, 'Ul');

    if (isDropdownParts(thisTag)) {
      return;
    }

    if (isDropdownParts(ev.target)) {
      return;
    }

    for (const hook of this.hooks) {
      hook.list.hide();
    }
  }

  _addData(trigger, data) {
    this._processData(trigger, data, 'addData');
  }

  _setData(trigger, data) {
    this._processData(trigger, data, 'setData');
  }

  _processData(trigger, data, methodName) {
    for (const hook of this.hooks) {
      if (hook.trigger.dataset.hasOwnProperty('id')) {
        if (hook.trigger.dataset.id === trigger) {
          hook.list[methodName](data);
        }
      }
    }
  }
}

export default DropLab;
