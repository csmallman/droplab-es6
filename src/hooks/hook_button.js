import Hook from './hook';

class HookButton extends Hook {
  constructor(trigger, list) {
    super(trigger, list);
    this.listen();
  }

  listen() {
    this.trigger.addEventListener('click', ev => {
      var buttonEvent = new CustomEvent('click.dl', {
        detail: {
          hook: self,
        },
        bubbles: true,
        cancelable: true
      });

      this.list.show();

      ev.target.dispatchEvent(buttonEvent);
    });
  }
};

export default HookButton;
