const types = {}

class HookFactory {
  static getHook(type, ...args) {
    const Constructor = types[type];
    return (Constructor ? new Constructor(...args) : null);
  }

  static registerHook(type, Constructor) {
    const proto = Constructor.prototype;

    if (proto.trgger && proto.list) {
      types[type] = Constructor;
    }
  }
}

export default HookFactory;
