import DropDown from '../dropdown';

class Hook {
  constructor(trigger, list) {
    this.trigger = trigger;
    this.list = new DropDown(list);
    this.id = trigger.dataset.id;
  }

  destroy() {
    this.list.destroy();
    this.trigger = null;
    this.list = null;
    this.id = null;
  }
}

export default Hook;
