import Hook from './hook';

class HookInput extends Hook {
  constructor(trigger, list) {
    super(trigger, list);
    this.listen();
  }

  handleEvent(ev) {
    const _this = this;
    const keyEvent = (ev, keyEventName) => {
      const keyEvent = new CustomEvent(keyEventName, {
        detail: {
          hook: self,
          text: ev.target.value,
          which: ev.which,
          key: ev.key,
        },
        bubbles: true,
        cancelable: true
      });
      ev.target.dispatchEvent(keyEvent);
      this.list.show();
    }

    switch (ev.type) {
      case 'mousedown':
        const mouseEvent = new CustomEvent('mousedown.dl', {
          detail: {
            hook: self,
            text: ev.target.value,
          },
          bubbles: true,
          cancelable: true
        });
        ev.target.dispatchEvent(mouseEvent);
        break;
      case 'input':
        const inputEvent = new CustomEvent('input.dl', {
          detail: {
            hook: self,
            text: ev.target.value,
          },
          bubbles: true,
          cancelable: true
        });
        ev.target.dispatchEvent(inputEvent);
        this.list.show();
        break;
      case 'keyup':
        keyEvent(ev, 'keyup.dl');
        break;
      case 'keydown':
        keyEvent(ev, 'keydown.dl');
        break;
      default:
        keyEvent(ev, 'click.dl');
        break;
    }
  }

  listen() {
    this.trigger.addEventListener('mousedown', this, false);
    this.trigger.addEventListener('input', this, false);
    this.trigger.addEventListener('keyup', this, false);
    this.trigger.addEventListener('keydown', this, false);
  }

  destroy() {
    super.destroy();
    this.trigger.removeEventListener('mousedown', this, false);
    this.trigger.removeEventListener('input', this, false);
    this.trigger.removeEventListener('keyup', this, false);
    this.trigger.removeEventListener('keydown', this, false);
  }
}

export default HookInput;
