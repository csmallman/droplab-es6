import template from './utils/template';

class DropDown {
  constructor(element) {
    this.hidden = true;
    this.element = element;
    this.items = this.getItems() || [];
    this.initialState = element.querySelector('[role="listItem"]').innerHTML;
    this.listen();
  }

  listen() {
    this.element.addEventListener('click', this, false);
  }

  handleEvent(ev) {
    if(ev.target.tagName === 'A' || ev.target.tagName === 'BUTTON') {
      ev.preventDefault();

      this.hide();

      const listEvent = new CustomEvent('click.dl', {
        detail: {
          list: self,
          selected: ev.target,
          data: ev.target.dataset,
        },
      });

      this.element.dispatchEvent(listEvent);
    }
  }

  getItems() {
    return [...this.element.querySelectorAll('li')];
  }

  toggle() {
    this[(this.hidden) ? 'show' : 'hide' ]();
  }

  setData(data) {
    this.data = data;
    this.render(data);
  }

  addData(data) {
    this.data = (this.data || []).concat(data);
    this.render(data);
  }

  render(data){
    this.destroyList();
    this.element.innerHTML = this.data.map(item => this.renderItem(item)).join('');
  }

  renderItem(item) {
    return template(this.initialState, item);
  }

  show() {
    this.element.style.display = 'block';
    this.hidden = false;
  }

  hide() {
    this.element.style.display = 'none';
    this.hidden = true;
  }

  destroyList() {
    while (this.element.firstChild) {
      if (this.element && this.element.dataset.hasOwnProperty('dynamic')) {
        this.element.removeChild(this.element.firstChild);
      }
    }
  }

  destroy() {
    this.destroyList();
    this.element.removeEventListener('click', this, false);
    this.element = null;
  }
};

export default DropDown;
