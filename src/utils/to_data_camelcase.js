import camelize from './camelize';

const toDataCamelCase = attr => camelize(attr.split('-').slice(1).join(' '));

export default toDataCamelCase;
