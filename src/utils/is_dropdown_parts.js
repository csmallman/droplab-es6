const isDropDownParts = target => {
  if(target.tagName === 'HTML') { return false; }
  return (target.hasAttribute('data-dropdown-trigger') || target.hasAttribute(' data-dropdown'));
};

export default isDropDownParts;
