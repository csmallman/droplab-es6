const template = (s, d) => {
  console.log('string', s);
  for(const p in d) {
    s = s.replace(new RegExp('{{'+ p +'}}','g'), d[p]);
  }
  return s;
};

export default template
