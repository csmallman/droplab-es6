const closest = (thisTag, stopTag) => {
  while(thisTag.tagName !== stopTag && thisTag.tagName !== 'HTML'){
    thisTag = thisTag.parentNode;
  }
  return thisTag;
};

export default closest;
