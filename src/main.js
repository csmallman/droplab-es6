import 'custom-event-polyfill';
import DropLab from './droplab';

document.addEventListener('DOMContentLoaded', () => {
  const droplab = new DropLab();
  const dropdownTriggers = [...document.querySelectorAll('[data-dropdown-trigger]')];

  droplab.plugin(DropLab => {
    const _addData = DropLab.prototype.addData;

    Object.assign(DropLab.prototype, {
      addData: function(...args) {
        console.log(args)// test plugin
        _addData.call(this, ...args);
      }
    })
  });

  droplab.addHooks(dropdownTriggers);

  droplab.addData('main', [{
    id: 24601,
    text: 'Jacob'
  },
  {
    id: 24602,
    text: 'Jeff'
  }]);

  droplab.addData('dynamic', [{
    id: 24601,
    text: 'John'
  },
  {
    id: 24602,
    text: 'Joe'
  }]);

  droplab.start();

  //droplab.destroy();
});
